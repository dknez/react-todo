import { Router } from 'express';
import {
  addTodo,
  deleteTodo,
  getTodos,
  updateTodo,
} from '../controllers/todos';

const todoRoutes: Router = Router();

todoRoutes.get('/todos', getTodos);
todoRoutes.post('/add-todo', addTodo);
todoRoutes.put('/edit-todo/:id', updateTodo);
todoRoutes.delete('/delete-todo/:id', deleteTodo);

export default todoRoutes;
